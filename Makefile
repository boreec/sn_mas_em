CC :=g++
CXXFLAGS :=-std=c++1z -O1 -O2 -Wall -Wextra
SRC_DIR := src
TARGETS := test

all: $(TARGETS)

test: $(SRC_DIR)/DataGenerator.o $(SRC_DIR)/Agent.o $(SRC_DIR)/EpidemicModelSolution.o $(SRC_DIR)/HeuristicSolutionFinder.o $(SRC_DIR)/EpidemicModel.o $(SRC_DIR)/CSVParser.o $(SRC_DIR)/main.o
	$(CC) -o $@ $^

$(SRC_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/Agent.o: $(SRC_DIR)/Agent.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/CSVParser.o: $(SRC_DIR)/CSVParser.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/EpidemicModel.o: $(SRC_DIR)/EpidemicModel.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/EpidemicModelSolution.o: $(SRC_DIR)/EpidemicModelSolution.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/HeuristicSolutionFinder.o: $(SRC_DIR)/HeuristicSolutionFinder.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/DataGenerator.o : $(SRC_DIR)/DataGenerator.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@
clean:
	rm -f $(SRC_DIR)/*.o

cleanall: clean
	rm -f $(TARGETS)
