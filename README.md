# SNMAS COVID-19

The purpose of this application is to modelize COVID-19 spread in France's population, using a multi-agent approach
in a social network environment. The comparison is made upon data provided by data.gouv.fr. This project is a part of my master degree, it has been supervised by Pr. Philippe Mathieu, leader of the SMAC research team in CRIStAL laboratory and framed by Université of Lille.

## Data

Data used are provided by [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/donnees-relatives-a-lepidemie-de-covid-19-en-france-vue-densemble/)

Time frame selected goes from March 2nd 2020 to July 1st 2020 (122 days). Before March there is not enough cases, after July the data collecting method changed.
The said data is stored in `/data/casesfrance_02032020_01072020.csv`.

## Compilation

Using the Makefile and with a C++17 suitable compiler simply do
```
$ make
```

To clean temporary object files in `src` directory :
``` 
$ make clean
```

To clean temporary object files and executables :
```
make cleanall
```

## Execution

The executable handle some arguments :
- `-a AGENTS_QUANTITY` : The size of graph that will be built.
- `-d DOT_FILE` : Export the graph in a dot file.
- `-h` : Display this help message.
- `-i CSV_FILE` : Load a CSV file containing data to converge to.
- `-p` : Display graph's adjacency list.
- `-P CSV_FILE` : Write simulations results to a csv file.
- `-s RUNS` : do `RUNS` simulations, if >= 1, the average is computed as a result.
- `-t` : Print execution time for the main steps.

Create a network with 1 000 000 agents, load data from a csv file and display execution time.
```
$ ./test -a 1000000 -i ../data/cases/france_02032020_01072020.csv -t
CSV parsing time                                 0.20ms
Graph's construction time                      320.17ms
Graph's traversal time                           8.94ms
```

To just create a graph with 100 agents and export it in a dotfile named *mygraph.dot*:
```
$ ./test -a 100 -d mygraph.dot
```

To run 1 simulation and export it to a CSV file :
```
$ ./test -P simulation.csv
```

To run 100 simulations on 10000 agents, do the average on results and export it to a CSV file :
```
$ ./test -a 10000 -P 100simulations.csv -s 100
```

To display the graph, you can use graphviz, which contain the **sfpd** tool :

```
$ sfdp -x -Goverlap=scale -Tpdf mygraph.dot > output.pdf
```

## Implementation details

### Parsing

The parsing simply uses GNU *getopt* function. See above for an explanation of arguments.

### Undirected Graph

The graph is undirected and does not have to contain informations (or weight) on the edges. However, the
nodes carry information since their represent an agent. Thus, the graph's class is generic and may be built
upon a specified data.

Inside the graph's class there are two data structures :
- a STL vector of Objects : It represents the nodes (A vector index is the node index).
- a 2D vector of unsigned integers : It represents the edges for each nodes. The first dimension is for the n nodes
index (so the 1D is as big as there are nodes in the graph), the second dimension contains the index of connected nodes to n_i.


## Contributors

- Cyprien Borée (CS student): main dev
- Philippe Mathieu (SMAC research team leader): supervisor
