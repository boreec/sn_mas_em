#ifndef __MAIN_HPP__
#define __MAIN_HPP__
/*
 * Display the given execution time for a given task name. Time is displayed is milliseconds, so if
 * execution is faster than 1ms, 0ms is printed. The final text attempts to be aligned on stdout.
 */
void print_execution_time(std::string step, std::chrono::duration<long int,std::ratio<1, 1000000000>> time){
  auto ms_time = time/std::chrono::microseconds(1000);
  auto dec = time/std::chrono::microseconds(1) % 100;
  std::string extra_zero = "";
  if(dec < 10)
    extra_zero = "0";
  unsigned step_len = step.size();
  unsigned width = 50;
  std::cout << step << std::setw(width - step_len) << ms_time << "." << dec << extra_zero << "ms" << std::endl;
}
#endif
