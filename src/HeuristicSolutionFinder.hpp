#ifndef __HEURISTIC_SOLUTION_FINDER_HPP__
#define __HEURISTIC_SOLUTION_FINDER_HPP__
#include <climits>
#include <iostream>
#include <random>
#include <vector>
#include "Agent.hpp"
#include "EpidemicModel.hpp"
#include "EpidemicModelSolution.hpp"
#include "UndirectedGraph.hpp"

/*
 * Contains a set of algorithms to find an EpidemicModelSolution.
 */
class HeuristicSolutionFinder{
private:
  /*
   * The amount of simulations done in the fitness function.
   * The more is done, the more precise solution's score is, but it
   * requires more computational power.
   * */
  const uint8_t FITNESS_AVERAGE_ITERATION = 10;
  std::vector<unsigned> _learning_curve;
  UndirectedGraph<Agent>* _graph;

  /*
   * Compute the score for a solution.
   * This will do FITNESS_AVERAGE_ITERATION simulations and compute the average before
   * running the score calculation.
   * ems : The solution to analyze.
   * average : The average of simulations returned in given a vector.
   * */
  unsigned fitness_function(const EpidemicModelSolution& ems, std::vector<unsigned>& average) const;

  void export_heuristic_result_to_csv(const std::string& filename, const std::vector<unsigned>& res) const;
public:

  /*
   * Store a given curve as a learning target. Every solution will be tested on a the provided
   * graph. If the given curve is empty, a runtime exception is thrown.
   * */
  HeuristicSolutionFinder(const std::vector<unsigned>& curve, UndirectedGraph<Agent>* graph);

  /*
   * This heuristic draws randoms parameters for a solution (using an uniform distribution),
   * computes its value with the fitness function and repeat this basic operation until
   * a number of iteration or a time limit is reached.
   * The returned value is the optimal solution found among all of the generated ones.
   * */
  EpidemicModelSolution randomHeuristic(const unsigned tries, const bool export_csv) const;

  /*
   * This heuristic divides the parameters space in "tries" part.
   * */
  EpidemicModelSolution iterativeHeuristic(const float step, const bool export_csv) const;
};
#endif
