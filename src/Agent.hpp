#ifndef __AGENT_HPP__
#define __AGENT_HPP__

#include <random>
#include <string>

enum class HealthState{
  SUSCEPTIBLE,
  INFECTIOUS,
  HOSPITALIZED,
  RECOVERED,
  DECEASED
};
  
class Agent{
private:
  static const uint8_t AGE_MIN = 1;
  static const uint8_t AGE_MAX = 115;
  
  uint8_t _age;
  bool _sex;   // 48.5% to be a man, 51.5% to be a woman.
  HealthState _hs;

  /*
   * Counter used to know how long an agent has been infected. When this counter reach
   * a specific constant, the agent health state will move to either RECOVERED or DECEASED.
   */
  uint16_t _infected_time;
public:

  /*
   * Create an agent with the given characteristics:
   * age : Agent's age (8 bits unsigned integer on [0;255])
   * sex : Agent's sex (bool)
   */
  Agent(const uint8_t age, const bool sex);

  /*
   * Create an Agent with random values for each characteristics.
   */
  Agent();
  std::string to_string() const;

  //getters
  uint8_t get_age() const;
  bool get_sex() const;
  uint16_t get_infected_time() const;

  bool is_susceptible() const;
  bool is_infectious() const;
  bool is_hospitalized() const;
  bool is_recovered() const;
  bool is_deceased() const;

  void set_health_state(HealthState hs);
  void set_infected_time(const uint16_t t);
  
};
#endif
