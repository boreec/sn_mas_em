#ifndef __EPIDEMIC_MODEL_SOLUTION_HPP__
#define __EPIDEMIC_MODEL_SOLUTION_HPP__
#include <string>
/*
 * Represents a solution for the epidemic model, i.e. a vector of parameters 
 * used during an epidemic simulation.
 */
class EpidemicModelSolution{
public:
  EpidemicModelSolution();
  float before_quarantine_alpha;
  float before_quarantine_beta;
  float before_quarantine_gamma;
  
  float during_quarantine_alpha;
  float during_quarantine_beta;
  float during_quarantine_gamma;
  
  float after_quarantine_alpha;
  float after_quarantine_beta;
  float after_quarantine_gamma;

  std::string to_string() const;
};
#endif
