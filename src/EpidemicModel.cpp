#include "EpidemicModel.hpp"

EpidemicModel::EpidemicModel(UndirectedGraph<Agent>* graph) : _graph(graph){
  clean_model();
}

void EpidemicModel::clean_model(){
  for(unsigned n = 0; n < _graph->size(); ++n){
    Agent a = _graph->get_node_value(n);
    a.set_health_state(HealthState::SUSCEPTIBLE);
    _graph->set_node_value(n,a);
  }
}

std::vector<std::vector<unsigned>> EpidemicModel::simulate_epidemy(const unsigned duration, const float alpha, const float beta, const float gamma){
  std::uniform_int_distribution<uint16_t> sickness_duration_distribution(SICKNESS_DURATION_MIN, SICKNESS_DURATION_MAX + 1);
  std::uniform_int_distribution<uint16_t> sickness_start_distribution(2, SICKNESS_DURATION_MIN + 1);
  std::vector<std::vector<unsigned>> results;
  std::vector<unsigned> deaths_per_day;
  std::vector<unsigned> infected_per_day;
  std::vector<unsigned> recovered_per_day;
  std::vector<unsigned> hospitalized_per_day;
  
  // set one node as infectious at the very beginning.
  std::uniform_int_distribution<unsigned> init_agent_distribution(0, _graph->size());
  unsigned random_agent = init_agent_distribution(generator);
  Agent a = _graph->get_node_value(random_agent);
  a.set_health_state(HealthState::INFECTIOUS);
  _graph->set_node_value(random_agent,a);

  // Initialize value vectors for day one
  deaths_per_day.push_back(0);
  recovered_per_day.push_back(0);
  hospitalized_per_day.push_back(0);
  infected_per_day.push_back(1);

  
  for(unsigned tick = 1; tick < duration; ++tick){

    // Compute agents new health state for the current tick.
    std::vector<unsigned> new_infected;
    std::vector<unsigned> new_deceased;
    std::vector<unsigned> new_recovered;
    std::vector<unsigned> new_hospitalized;
    for(unsigned n_idx = 0; n_idx < _graph->size(); ++n_idx){
      Agent agent = _graph->get_node_value(n_idx);
      std::vector<unsigned> neighbours = _graph->get_neighbours(n_idx);
      bool agent_is_newly_infected = false;

      // If agent is SUSCEPTIBLE, he can become INFECTIOUS if one of his neighbour is also INFECTIOUS.
      if(agent.is_susceptible()){
	unsigned neighbour_idx = 0;
	while(neighbour_idx < neighbours.size() && !agent_is_newly_infected){
	  Agent neighbour = _graph->get_node_value(neighbours[neighbour_idx]);
	  if(neighbour.is_infectious() && neighbour.get_infected_time() > sickness_start_distribution(generator)){
	    
	    float theta_1 = alpha * raw_transmission_prob(agent.get_age(),neighbour.get_age());

	    std::bernoulli_distribution distribution(theta_1);
	    if(distribution(generator)){
	      new_infected.push_back(n_idx);
	      agent_is_newly_infected = true;
	    }
	  }
	  neighbour_idx++;
	}
      }else if(agent.is_infectious()){
	if(agent.get_infected_time() < sickness_duration_distribution(generator)){
	  agent.set_infected_time(agent.get_infected_time() + 1);
	  _graph->set_node_value(n_idx, agent);
	}

	// If agent is INFECTIOUS, he can become either HOSPITALIZED or RECOVERED after a while.
	else{
	  float theta_2 = beta * raw_hospitalized_prob(agent.get_age(), agent.get_infected_time());
	  std::bernoulli_distribution hospitalized_distribution(theta_2);
	  if(hospitalized_distribution(generator)){
	    new_hospitalized.push_back(n_idx);
	  }else{
	    new_recovered.push_back(n_idx);
	  }
	}
      }
      // If agent is HOSPITALIZED, he can become either DECEASED or RECOVERED after a while.
      else if(agent.is_hospitalized()){
	float theta_3 = gamma * raw_deceased_prob(agent.get_age(), agent.get_infected_time(), agent.get_sex());
	std::bernoulli_distribution deceased_distribution(theta_3);
	if(deceased_distribution(generator)){
	  new_deceased.push_back(n_idx);
	}else{
	  new_recovered.push_back(n_idx);
	}
      }
    
    }
    
    /* Apply computed new states to agents. */
    // Every new infected agents has its infected time set to 0 and
    // its health state changed to INFECTED.
    for(const auto& n : new_infected){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::INFECTIOUS);
      agent.set_infected_time(0);
      _graph->set_node_value(n, agent);
    }
    // Every new deceased agents has its health state changed to DECEASED.
    for(const auto &n : new_deceased){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::DECEASED);
      _graph->set_node_value(n,agent);
    }
    // Every new recovered agents has its health state changed to RECOVERED.
    for(const auto &n : new_recovered){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::RECOVERED);
      _graph->set_node_value(n,agent);
    }
    // Every new hospitalized agents has its infected time increased and
    // its health state changed to HOSPITALIZED.
    for(const auto &n : new_hospitalized){
      Agent agent = _graph->get_node_value(n);
      agent.set_infected_time(agent.get_infected_time() + 1);
      agent.set_health_state(HealthState::HOSPITALIZED);
      _graph->set_node_value(n,agent);
    }
    deaths_per_day.push_back(new_deceased.size());
    infected_per_day.push_back(new_infected.size());
    recovered_per_day.push_back(new_recovered.size());
    hospitalized_per_day.push_back(new_hospitalized.size());
  }
  results.push_back(infected_per_day);
  results.push_back(recovered_per_day);
  results.push_back(hospitalized_per_day);
  results.push_back(deaths_per_day);
  return results;
}

std::vector<std::vector<unsigned>> EpidemicModel::simulate_epidemy(const uint8_t duration, const EpidemicModelSolution& ems){
  std::uniform_int_distribution<uint16_t> sickness_duration_distribution(SICKNESS_DURATION_MIN, SICKNESS_DURATION_MAX + 1);
  std::uniform_int_distribution<uint16_t> sickness_start_distribution(2, SICKNESS_DURATION_MIN + 1);
  std::vector<std::vector<unsigned>> results;
  std::vector<unsigned> deaths_per_day;
  std::vector<unsigned> infected_per_day;
  std::vector<unsigned> recovered_per_day;
  std::vector<unsigned> hospitalized_per_day;
  
  // set one node as infectious at the very beginning.
  std::uniform_int_distribution<unsigned> init_agent_distribution(0, _graph->size());
  unsigned random_agent = init_agent_distribution(generator);
  Agent a = _graph->get_node_value(random_agent);
  a.set_health_state(HealthState::INFECTIOUS);
  _graph->set_node_value(random_agent,a);

  // Initialize value vectors for day one
  deaths_per_day.push_back(0);
  recovered_per_day.push_back(0);
  hospitalized_per_day.push_back(0);
  infected_per_day.push_back(1);
  float alpha, beta, gamma;
  
  for(uint8_t tick = 1; tick < duration; ++tick){
    if(tick < 40){
      alpha = ems.before_quarantine_alpha;
      beta = ems.before_quarantine_beta;
      gamma = ems.before_quarantine_beta;
    }
    else if(tick < 80){
      alpha = ems.during_quarantine_alpha;
      beta = ems.during_quarantine_beta;
      gamma = ems.during_quarantine_beta;

    }else{
      alpha = ems.after_quarantine_alpha;
      beta = ems.after_quarantine_beta;
      gamma = ems.after_quarantine_beta;

    }
    // Compute agents new health state for the current tick.
    std::vector<unsigned> new_infected;
    std::vector<unsigned> new_deceased;
    std::vector<unsigned> new_recovered;
    std::vector<unsigned> new_hospitalized;
    for(unsigned n_idx = 0; n_idx < _graph->size(); ++n_idx){
      Agent agent = _graph->get_node_value(n_idx);
      std::vector<unsigned> neighbours = _graph->get_neighbours(n_idx);
      bool agent_is_newly_infected = false;

      // If agent is SUSCEPTIBLE, he can become INFECTIOUS if one of his neighbour is also INFECTIOUS.
      if(agent.is_susceptible()){
	unsigned neighbour_idx = 0;
	while(neighbour_idx < neighbours.size() && !agent_is_newly_infected){
	  Agent neighbour = _graph->get_node_value(neighbours[neighbour_idx]);
	  if(neighbour.is_infectious() && neighbour.get_infected_time() > sickness_start_distribution(generator)){
	    
	    float theta_1 = alpha * raw_transmission_prob(agent.get_age(),neighbour.get_age());

	    std::bernoulli_distribution distribution(theta_1);
	    if(distribution(generator)){
	      new_infected.push_back(n_idx);
	      agent_is_newly_infected = true;
	    }
	  }
	  neighbour_idx++;
	}
      }else if(agent.is_infectious()){
	if(agent.get_infected_time() < sickness_duration_distribution(generator)){
	  agent.set_infected_time(agent.get_infected_time() + 1);
	  _graph->set_node_value(n_idx, agent);
	}

	// If agent is INFECTIOUS, he can become either HOSPITALIZED or RECOVERED after a while.
	else{
	  float theta_2 = beta * raw_hospitalized_prob(agent.get_age(), agent.get_infected_time());
	  std::bernoulli_distribution hospitalized_distribution(theta_2);
	  if(hospitalized_distribution(generator)){
	    new_hospitalized.push_back(n_idx);
	  }else{
	    new_recovered.push_back(n_idx);
	  }
	}
      }
      // If agent is HOSPITALIZED, he can become either DECEASED or RECOVERED after a while.
      else if(agent.is_hospitalized()){
	float theta_3 = gamma * raw_deceased_prob(agent.get_age(), agent.get_infected_time(), agent.get_sex());
	std::bernoulli_distribution deceased_distribution(theta_3);
	if(deceased_distribution(generator)){
	  new_deceased.push_back(n_idx);
	}else{
	  new_recovered.push_back(n_idx);
	}
      }
    
    }
    
    /* Apply computed new states to agents. */
    // Every new infected agents has its infected time set to 0 and
    // its health state changed to INFECTED.
    for(const auto& n : new_infected){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::INFECTIOUS);
      agent.set_infected_time(0);
      _graph->set_node_value(n, agent);
    }
    // Every new deceased agents has its health state changed to DECEASED.
    for(const auto &n : new_deceased){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::DECEASED);
      _graph->set_node_value(n,agent);
    }
    // Every new recovered agents has its health state changed to RECOVERED.
    for(const auto &n : new_recovered){
      Agent agent = _graph->get_node_value(n);
      agent.set_health_state(HealthState::RECOVERED);
      _graph->set_node_value(n,agent);
    }
    // Every new hospitalized agents has its infected time increased and
    // its health state changed to HOSPITALIZED.
    for(const auto &n : new_hospitalized){
      Agent agent = _graph->get_node_value(n);
      agent.set_infected_time(agent.get_infected_time() + 1);
      agent.set_health_state(HealthState::HOSPITALIZED);
      _graph->set_node_value(n,agent);
    }
    deaths_per_day.push_back(new_deceased.size());
    infected_per_day.push_back(new_infected.size());
    recovered_per_day.push_back(new_recovered.size());
    hospitalized_per_day.push_back(new_hospitalized.size());
  }
  results.push_back(infected_per_day);
  results.push_back(recovered_per_day);
  results.push_back(hospitalized_per_day);
  results.push_back(deaths_per_day);
  return results;
}

float EpidemicModel::raw_transmission_prob(const unsigned age_susceptible, const unsigned age_infected) const{
  return std::log2((float)age_infected) / std::log2((float)age_susceptible + (float)age_infected);
}

float EpidemicModel::raw_hospitalized_prob(const unsigned agent_age, const unsigned agent_time_infected) const{
  return (1 - 1/agent_time_infected) * (std::log2((float)agent_age) / std::log2(115));
}

float EpidemicModel::raw_deceased_prob(const unsigned agent_age, const unsigned agent_time_infected, const bool agent_sex) const{
  return (1 - 1/agent_time_infected) * (std::log2((float)agent_age) / std::log2(115)) * pow(0.6,!agent_sex);
}

void EpidemicModel::to_csv(const std::string filename, const std::vector<std::vector<unsigned>> simulation_results) const{
  std::ofstream output_file(filename);
  if(!output_file.is_open()){
    throw std::runtime_error("Error opening a file for CSV writing: " + filename);
  }

  output_file << "ticks,new_infections,new_recovered,new_hospitalized,new_deaths\n";
  for(unsigned tick = 0; tick < simulation_results[0].size(); ++tick){
    output_file << tick << "," << simulation_results[0][tick];
    for(unsigned v_idx = 1; v_idx < simulation_results.size(); ++ v_idx){
      output_file << "," << simulation_results[v_idx][tick];
    }
    output_file << "\n";
  }
  
  output_file.close();
}
