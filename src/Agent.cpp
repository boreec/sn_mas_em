#include "Agent.hpp"
#include <iostream>

Agent::Agent(const uint8_t age, const bool sex):
  _age(age), _sex(sex), _hs(HealthState::SUSCEPTIBLE)
{
  // :)
}

Agent::Agent(){
  std::random_device generator;
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  std::uniform_int_distribution<int> age_distribution(AGE_MIN, AGE_MAX);

  _age = age_distribution(generator);
  _sex = (distribution(generator) < 0.515);
  _hs = HealthState::SUSCEPTIBLE;
}

std::string Agent::to_string() const {
  std::string result = "";
  result += std::to_string(_age) + "yo, ";
  result += _sex ? "W, " : "M, ";
  switch(_hs){
  case HealthState::SUSCEPTIBLE :
    result += "HT : SUSCEPTIBLE";
    break;
  case HealthState::INFECTIOUS :
    result += "HT : INFECTIOUS";
    break;
  case HealthState::HOSPITALIZED :
    result += "HT : HOSPITALIZED";
    break;
  case HealthState::RECOVERED :
    result += "HT : RECOVERED";
    break;
  case HealthState::DECEASED :
    result += "HT : DECEASED";
    break;
  default:
    result += "HT : UNKNOWN";
    break;
  }
  return result; 
}

uint8_t Agent::get_age() const{
  return _age;
}

bool Agent::get_sex() const{
  return _sex;
}

uint16_t Agent::get_infected_time() const{
  return _infected_time;
}

bool Agent::is_susceptible() const{
  return _hs == HealthState::SUSCEPTIBLE;
}

bool Agent::is_infectious() const{
  return _hs == HealthState::INFECTIOUS;
}

bool Agent::is_hospitalized() const{
  return _hs == HealthState::HOSPITALIZED;
}

bool Agent::is_recovered() const{
  return _hs == HealthState::RECOVERED;
}

bool Agent::is_deceased() const{
  return _hs == HealthState::DECEASED;
}

void Agent::set_health_state(HealthState hs){
  _hs = hs;
}

void Agent::set_infected_time(const uint16_t t){
  _infected_time = t;
}
