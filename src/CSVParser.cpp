#include "CSVParser.hpp"

std::map<std::string, std::vector<unsigned>> CSVParser::parse(const std::string filename){
  std::map<std::string, std::vector<unsigned>> result;
  std::map<unsigned, std::string> col_index;
  
  std::ifstream csv_file(filename);
  if(!csv_file.is_open()){
    std::cerr << "Error opening file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  std::string cell_value;
  unsigned line_number = 0;
  while(getline(csv_file, line)){
    std::stringstream linestream(line);
    unsigned col = 0;
    while(std::getline(linestream, cell_value, ',')){
      if(line_number == 0){
	std::vector<unsigned> v;
        result[cell_value] = v;
	col_index[col] = cell_value;
      }else{
	std::string column_name = col_index[col];
	unsigned cell_number = 0;
	cell_number = std::stoul(cell_value);
	result[column_name].push_back(cell_number);
      }
      col++;
    }
    line_number++;
  }
  csv_file.close();
  return result;
}
