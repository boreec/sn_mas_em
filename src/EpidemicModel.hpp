#ifndef __EPIDEMIC_MODEL_HPP__
#define __EPIDEMIC_MODEL_HPP__
#include <cmath>
#include <fstream>
#include <random>
#include <string>
#include <vector>
#include "Agent.hpp"
#include "EpidemicModelSolution.hpp"
#include "UndirectedGraph.hpp"
class EpidemicModel{
private:
  UndirectedGraph<Agent>* _graph;
  std::default_random_engine generator;
  const uint16_t SICKNESS_DURATION_MIN = 7;
  const uint16_t SICKNESS_DURATION_MAX = 14;

public:
  EpidemicModel(UndirectedGraph<Agent>* graph);

  /*
   * Compute a raw probability for the transmission between two agents based on their age.
   * Error can happen if one of the both age is either 1 or 0, these cases must be treated
   * somewhere else.
   */
  float raw_transmission_prob(const unsigned age_susceptible, const unsigned age_infected) const;

  /*
   * Compute a raw probability for an agennt to transition from INFECTIOUS to HOSPITALIZED.
   */
  float raw_hospitalized_prob(const unsigned agent_age, const unsigned agent_time_infected) const;

  /*
   * Compute a raw probability for an agent to transition from HOSPITALIZED to DECEASED.
   */
  float raw_deceased_prob(const unsigned agent_age, const unsigned agent_time_infected, const bool agent_sex) const;
  /*
    Simulate the spread of a disease on network of agents.
    duration : the total time in ticks for the simulation.
    alpha : factor affecting the transmission.
    beta : factor affecting the hospitalized/recovered.
    gamma : factor affecting the deceased/recovered.

    The results of this function is a 2D unsigned vector where the 1st dimension
    corresponding to the number of different results returned (deaths, infections...)
    and the 2nd dimension corresponding to the simulation duration in ticks.
   */
  std::vector<std::vector<unsigned>> simulate_epidemy(const unsigned duration, const float alpha, const float beta, const float gamma);

  /*
   * Simulate the spread of a disease on network of agents.
   * Duration : the total time in ticks for the simulation.
   * ems : Contains the coefficients to configure the parameters during the simulation.
   * */
  std::vector<std::vector<unsigned>> simulate_epidemy(const uint8_t duration, const EpidemicModelSolution& ems);

  /*
   * Reset HealthState for every agent in the graph.
   */
  void clean_model();
  /*
    Export a simulation results into a CSV file.
  */
  void to_csv(const std::string filename, const std::vector<std::vector<unsigned>> simulation_results) const;
};
#endif
 
