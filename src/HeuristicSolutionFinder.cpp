#include "HeuristicSolutionFinder.hpp"

HeuristicSolutionFinder::HeuristicSolutionFinder(const std::vector<unsigned>& curve, UndirectedGraph<Agent>* graph)
{
  if(curve.empty())
    throw std::runtime_error("HeuristicSolutionFinder : Learning curve is empty.");
  _learning_curve = curve;
  _graph = graph;
}

unsigned HeuristicSolutionFinder::fitness_function(const EpidemicModelSolution& ems, std::vector<unsigned>& average) const{
  EpidemicModel em(_graph);
  for(uint8_t s = 0; s < FITNESS_AVERAGE_ITERATION; ++s){
    em.clean_model();

    std::vector<std::vector<unsigned>> em_results = em.simulate_epidemy(120, ems);

    if(average.empty()){
      average.insert(average.end(), em_results[0].begin(), em_results[0].end());
    }else{
      for(uint8_t i = 0; i < em_results[0].size(); ++i){
	average[i] += em_results[0][i];
      }
    }
  }

  if(_learning_curve.size() != average.size()){
    throw std::runtime_error("Learning data is not made of exactly 120 points. Canceling.");
  }

  // Do the simulations mean.
  for(uint8_t i = 0; i < average.size(); ++i){
    average[i] /= FITNESS_AVERAGE_ITERATION;
  }

  unsigned mean_square = 0;
  for(uint8_t i = 0; i < average.size(); ++i){
    mean_square += pow((int)average[i] - (int)_learning_curve[i], 2);
  } 
  return mean_square;
}

EpidemicModelSolution HeuristicSolutionFinder::randomHeuristic(const unsigned tries,const bool export_csv) const {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.0, 1.0);
  std::vector<unsigned> best_average_simulation;
  EpidemicModelSolution best_ems, ems;
  unsigned best_score = UINT_MAX;
  
  for(unsigned t = 0; t < tries; ++t){
    std::vector<unsigned> average_simulation;
    ems.before_quarantine_alpha = dis(gen);
    ems.before_quarantine_beta = dis(gen);
    ems.before_quarantine_gamma = dis(gen);
    ems.during_quarantine_alpha = dis(gen);
    ems.during_quarantine_beta = dis(gen);
    ems.during_quarantine_gamma = dis(gen);
    ems.after_quarantine_alpha = dis(gen);
    ems.after_quarantine_beta = dis(gen);
    ems.after_quarantine_gamma = dis(gen);

    unsigned score = fitness_function(ems, average_simulation);
    if(score < best_score){
      best_score = score;
      best_ems = ems;
      best_average_simulation = average_simulation;
    }
    //std::cout << t << "/" << tries << std::endl;
  }
  if(export_csv){
    export_heuristic_result_to_csv("HEURISTIC_RANDOM_" + std::to_string(tries) + ".csv", best_average_simulation);
  }
  std::cout << "best_score : " << best_score << std::endl;
  return best_ems;
}

EpidemicModelSolution HeuristicSolutionFinder::iterativeHeuristic(const float step,const bool export_csv) const {
  EpidemicModelSolution best_ems, ems;
  unsigned best_score = UINT_MAX;
  std::vector<unsigned> best_average_simulation;
  for(float a1 = step; a1 <= 1; a1 = a1 + step){
    for(float a2 = step; a2 <= 1; a2 = a2 + step){
      for(float a3 = step; a3 <= 1; a3 = a3 + step){
	std::vector<unsigned> average_simulation;
	ems.before_quarantine_alpha = a1;
	ems.during_quarantine_alpha = a2;
	ems.after_quarantine_alpha = a3;

	unsigned score = fitness_function(ems, average_simulation);
	if(score < best_score){
	  best_score = score;
	  best_ems = ems;
	  best_average_simulation = average_simulation;
	}
	//std::cout << std::to_string(a1) << "/1.0\t";
	//std::cout << std::to_string(a2) << "/1.0\t";
	//std::cout << std::to_string(a3) << "/1.0" << std::endl;
      }
    }
  }

  if(export_csv){
    export_heuristic_result_to_csv("HEURISTIC_ITERATIVE_" + std::to_string(step) + ".csv", best_average_simulation);
  }
  std::cout << "best_score : " << best_score << std::endl;
  return best_ems;
}


void HeuristicSolutionFinder::export_heuristic_result_to_csv(const std::string& filename, const std::vector<unsigned>& res) const{
  std::ofstream output_file(filename);

  if(!output_file.is_open()){
    throw std::runtime_error("Error opening CSV file to write heuritistic results: " + filename);
  }

  output_file << "learning_data,heuristic_solution\n";
  for(uint8_t i = 0; i < _learning_curve.size(); ++i){
    output_file << _learning_curve[i] << "," << res[i] << "\n";
  }
}
