#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>
#include <unistd.h>
#include "Agent.hpp"
#include "BarabasiAlbertGraphBuilder.hpp"
#include "CSVParser.hpp"
#include "DataGenerator.hpp"
#include "EpidemicModel.hpp"
#include "HeuristicSolutionFinder.hpp"
#include "main.hpp"
#include "UndirectedGraph.hpp"

void print_usage(){
  std::cout << "-a AGENTS_QUANTITY:\tCreate graph with a given quantity of agents." << std::endl;
  std::cout << "-A FLOAT_VALUE    :\tSet the alpha parameter. By default it's 0.5." << std::endl;
  std::cout << "-B FLOAT_VALUE    :\tSet the beta parameter. By default it's 0.5." << std::endl;
  std::cout << "-g CURVE_TYPE     :\tGenerate data to converge to." << std::endl;
  std::cout << "-G FLOAT_VALUE    :\tSet the gamma parameter. By default it's 0.5" << std::endl;
  std::cout << "-h                :\tDisplay this message." << std::endl;
  std::cout << "-H HEURISTIC      :\tUse a defined heuristic to learn a curve." << std::endl;
  std::cout << "-i CSV_FILE       :\tData used to converge to." << std::endl;
  std::cout << "-I UNSIGNED_VALUE :\tNumber of iterations to do in heuristic." << std::endl;
  std::cout << "-d <filename.dot> :\tWrite graph as dot file." << std::endl;
  std::cout << "-p                :\tPrint graph's adjency matrix in stdout." << std::endl;
  std::cout << "-P CSV_FILE       :\tWrite simulation results to a csv file." << std::endl;
  std::cout << "-s RUNS           :\tDo RUNS simulations, if >= 1, the average is computed as a result." << std::endl;
  std::cout << "-t                :\tPrint execution time in ms for main steps." << std::endl;
}

void parse_args(int argc, char* argv[]){
  std::vector<unsigned> learning_data;
  unsigned AGENTS_QUANTITY = 10;
  std::string GRAPH_FILENAME = "";
  std::string CSV_FILENAME = "";
  bool PRINT_EXECUTION_TIME = false;
  bool PRINT_ADJACENCY_LIST = false;
  bool PRINT_USAGE_ARG = false;
  bool GENERATE_DATA = false;
  std::string PROPAGATE_CSV = "";
  unsigned PROPAGATE_RUNS = 1;
  CURVE_TYPE ct;
  float alpha = 0.5;
  float beta = 0.5;
  float gamma = 0.5;
  std::string HEURISTIC = "";
  unsigned HEURISTIC_IT = 10;
  
  int opt;
  while ((opt = getopt(argc,argv,"a:A:B:g:G:hH:i:I:d:pP:s:t")) != EOF){
    switch(opt){
    case 'a':
      AGENTS_QUANTITY = std::stoul(optarg);
      break;
    case 'A' :
      alpha = std::stof(optarg);
      break;
    case 'B' :
      beta = std::stof(optarg);
      break;
    case 'd' :
      GRAPH_FILENAME = optarg;
      break;
    case 'g':
      GENERATE_DATA = true;
      if(optarg == std::string("GROWTH")){
	ct = CURVE_TYPE::GROWTH;
      }else if(optarg == std::string("ONE_BUMP")){
	ct = CURVE_TYPE::ONE_BUMP;
      }else if(optarg == std::string("TWO_BUMPS")){
	ct = CURVE_TYPE::TWO_BUMPS;
      }else{
	std::cerr << "Unrecognized curve type, it's either \"LINEAR_GROW\", \"ONE_BUMP\" or \"TWO_BUMPS\"." << std::endl;
	return;
      }
      break;
    case 'G':
      gamma = std::stof(optarg);
      break;
    case 'h' :
      PRINT_USAGE_ARG = true;
    break;
    case 'H':
      HEURISTIC = optarg;
      break;
    case 'i':
      CSV_FILENAME=optarg;
      break;
    case 'I':
      HEURISTIC_IT = std::stoul(optarg);
      break;
    case 'p' :
      PRINT_ADJACENCY_LIST = true;
    break;
    case 'P':
      PROPAGATE_CSV = optarg;
    break;
    case 't':
      PRINT_EXECUTION_TIME = true;
    break;
    case 's':
      PROPAGATE_RUNS = std::stoul(optarg);
      break;
    case '?': 
      print_usage();
      break;
    default:
      print_usage();
      break;
    }
  }

  // Handle incompatible arguments.
  if(GENERATE_DATA && CSV_FILENAME != ""){
    throw std::runtime_error("Incompatible arguments (-g/-i): To use learning data you can only generate data or import one, but not do both at same time!");
  }
  if(HEURISTIC != "" && !GENERATE_DATA && CSV_FILENAME == ""){
    throw std::runtime_error("You can't use heuristics (-H) without specifying learning data (-g/-i).");
  }

  if(CSV_FILENAME != ""){
    auto parse_begin = std::chrono::high_resolution_clock::now();
    /*
     * data["total_deces"] -> vector of deaths per day.
     */
    std::map<std::string, std::vector<unsigned>> data = CSVParser::parse(CSV_FILENAME);
    auto parse_end = std::chrono::high_resolution_clock::now();
    if(PRINT_EXECUTION_TIME)
      print_execution_time("CSV parsing time", parse_end - parse_begin);
  }
  if(GENERATE_DATA){
    unsigned qt = .01 * AGENTS_QUANTITY;
    DataGenerator dg(ct, qt);
    learning_data = dg.generate(0,120);
  }
  std::vector<Agent> agents;
  agents.reserve(AGENTS_QUANTITY);
  for(unsigned a = 0; a < AGENTS_QUANTITY; ++a){
    Agent ag;
    agents.push_back(ag);
  }

  UndirectedGraph<Agent>* graph = BarabasiAlbertGraphBuilder<Agent>::build(agents, PRINT_EXECUTION_TIME);
    
  if(PRINT_USAGE_ARG)
    print_usage();

  if(PRINT_ADJACENCY_LIST)
    std::cout << graph->to_string() << std::endl;

  if(GRAPH_FILENAME.size())
    graph->export_dot(GRAPH_FILENAME);

  if(PROPAGATE_CSV != ""){
    std::vector<std::vector<unsigned>> average;
    EpidemicModel em(graph);
    for(unsigned r = 0; r < PROPAGATE_RUNS; ++r){
      em.clean_model();
      std::vector<std::vector<unsigned>> simulation_results = em.simulate_epidemy(120, alpha, beta, gamma);
      if(average.size() == 0){
	for(unsigned results = 0; results < simulation_results.size(); ++results){
	  average.push_back(simulation_results[results]);
	}
      }else{
	for(unsigned results = 0; results < simulation_results.size(); ++results){
	  for(unsigned rr = 0; rr < simulation_results[results].size(); ++rr){
	    average[results][rr] += simulation_results[results][rr];
	  }
	}
      }
    }
    for(unsigned results = 0; results < average.size(); ++results){
      for(unsigned rr = 0; rr < average[results].size(); ++rr){
	if(average[results][rr])
	  average[results][rr] /= PROPAGATE_RUNS;
      }
    }
    em.to_csv(PROPAGATE_CSV, average);
  }
  if(HEURISTIC != ""){
    HeuristicSolutionFinder hsf(learning_data, graph);
    EpidemicModelSolution best_solution;
    if(HEURISTIC == "RANDOM"){
      best_solution = hsf.randomHeuristic(HEURISTIC_IT, true);
    }else if(HEURISTIC == "ITERATIVE"){
      best_solution = hsf.iterativeHeuristic(1.0 / HEURISTIC_IT, true);
    }else if(HEURISTIC == "BENCHMARK"){
      
      std::cout << "HEURISTIC : RANDOM" << std::endl;
      auto rand_10_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.randomHeuristic(10, true);
      auto rand_10_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_RAND_10", rand_10_end - rand_10_beg);
      }
      
      auto rand_100_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.randomHeuristic(100, true);
      auto rand_100_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_RAND_100", rand_100_end - rand_100_beg);
      }
      auto rand_1000_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.randomHeuristic(1000, true);
      auto rand_1000_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_RAND_1000", rand_1000_end - rand_1000_beg);
      }
      
      std::cout << "HEURISTIC : ITERATIVE" << std::endl;
      auto iterative_5_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.iterativeHeuristic(1.0 / 5.0, true);
      auto iterative_5_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_ITER_STEP0.2", iterative_5_end - iterative_5_beg);
      }
      
      auto iterative_10_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.iterativeHeuristic(1.0 / 10.0, true);
      auto iterative_10_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_ITER_STEP0.1", iterative_10_end - iterative_10_beg);
      }

      auto iterative_20_beg = std::chrono::high_resolution_clock::now();
      best_solution = hsf.iterativeHeuristic(1.0 / 20.0, true);
      auto iterative_20_end = std::chrono::high_resolution_clock::now();
      std::cout << best_solution.to_string() << std::endl;
      if(PRINT_EXECUTION_TIME){
	print_execution_time("HEURISTIC_ITER_STEP0.05", iterative_20_end - iterative_20_beg);
      }

    }else{
      throw std::runtime_error("Unknown heuristic!");
    }
  }
  delete graph;
}
int main(int argc, char* argv[]){
  parse_args(argc, argv);
  return 0;
}
