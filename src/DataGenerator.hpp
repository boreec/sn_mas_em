#ifndef __DATA_GENERATOR_HPP__
#define __DATA_GENERATOR_HPP__
#include <cmath>
#include <stdexcept>
#include <vector>
/*
 * The kind of curve to generate.
 */
enum class CURVE_TYPE{
  GROWTH,
  ONE_BUMP,
  TWO_BUMPS
};

/*
 * Generate a requested number of fictious data points under
 * certain conditions.
 */
class DataGenerator{
private:
  CURVE_TYPE _ct;
  unsigned _quantity;
  
  double growth(const double x) const;
  double one_bump(const double x) const;
  double two_bumps(const double x) const;
  constexpr double pi() const { return std::atan(1)*4;}
public:
  DataGenerator(const CURVE_TYPE ct, const unsigned quantity);
  std::vector<unsigned> generate(unsigned begin, unsigned end) const;
};
#endif
