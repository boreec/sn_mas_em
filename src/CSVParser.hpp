#ifndef __CSV_PARSER_HPP__
#define __CSV_PARSER_HPP__
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
class CSVParser{
  
public:
  /*
   * Parse a CSV file and return a map where the key is a string to a column, and the
   * value is a vector of unsigned integers found in the the column cells. If the cell
   * is empty, 0 is placed in the vector.
   */
  static std::map<std::string, std::vector<unsigned>> parse(const std::string filename);
};
#endif
