#include "EpidemicModelSolution.hpp"

EpidemicModelSolution::EpidemicModelSolution()
{
  before_quarantine_alpha = 0.;
  before_quarantine_beta = 0.;
  before_quarantine_gamma = 0.;

  during_quarantine_alpha = 0.;
  during_quarantine_beta = 0.;
  during_quarantine_gamma = 0.;

  after_quarantine_alpha = 0.;
  after_quarantine_beta = 0.;
  after_quarantine_gamma = 0.;
}

std::string EpidemicModelSolution::to_string() const{
  std::string result = "";
  result += "before_quarantine_alpha : " + std::to_string(before_quarantine_alpha) + "\n";
  result += "before_quarantine_beta : " + std::to_string(before_quarantine_beta) + "\n";
  result += "before_quarantine_gamma : " + std::to_string(before_quarantine_gamma) + "\n";
  result += "during_quarantine_alpha : " + std::to_string(during_quarantine_alpha) + "\n";
  result += "during_quarantine_beta : " + std::to_string(during_quarantine_beta) + "\n";
  result += "during_quarantine_gamma : " + std::to_string(during_quarantine_gamma) + "\n";
  result += "after_quarantine_alpha : " + std::to_string(after_quarantine_alpha) + "\n";
  result += "after_quarantine_beta : " + std::to_string(after_quarantine_beta) + "\n";
  result += "after_quarantine_gamma : " + std::to_string(after_quarantine_gamma);
  return result;
}
