#include "DataGenerator.hpp"

DataGenerator::DataGenerator(const CURVE_TYPE ct, const unsigned quantity) : _ct(ct), _quantity(quantity){
  //
}

double DataGenerator::growth(const double x) const{
  return _quantity * pow(cos(1./77. * x),2);
}

double DataGenerator::one_bump(const double x) const{
  return _quantity * pow(cos(1./39. * x),2);
}

double DataGenerator::two_bumps(const double x) const{
  return _quantity * pow(cos(1./19. * x),2);
}

std::vector<unsigned> DataGenerator::generate(unsigned begin, unsigned end) const{
  std::vector<unsigned> result;
  for(unsigned i = begin; i < end; ++i){
    switch(_ct){
    case CURVE_TYPE::GROWTH:
      result.insert(result.begin(),growth(i - 77 * pi()));
      break;
    case CURVE_TYPE::ONE_BUMP:
      result.push_back(one_bump(i + 20*pi()));
      break;
    case CURVE_TYPE::TWO_BUMPS:
      result.push_back(two_bumps(i - 10*pi()));
      break;
    default:
      throw std::runtime_error("Unexpected error : Unknown curve type");
    }
  }
  return result;
}
