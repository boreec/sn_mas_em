#ifndef __BARABASI_ALBERT_GRAPH_BUILDER_HPP__
#define __BARABASI_ALBERT_GRAPH_BUILDER_HPP__
#include <iostream>
#include <random>
#include "main.hpp"
#include "UndirectedGraph.hpp"

template <class T>
class BarabasiAlbertGraphBuilder{
public:

  /*
   * Build a graph according to the Barabasi-Albert model.
   * nodes : vector of object stored in each node.
   * print_execution : display execution time or not in stdout.
   * */
  static UndirectedGraph<T>* build(const std::vector<T>& nodes, const bool print_execution){
    auto g_cons_begin = std::chrono::high_resolution_clock::now();
    UndirectedGraph<T>* graph = new UndirectedGraph<T>();

    if(nodes.empty()){
      auto g_cons_end = std::chrono::high_resolution_clock::now();
      if(print_execution)
	print_execution_time("Graph's construction time", g_cons_end - g_cons_begin);
      return graph;
    }
    
    std::random_device rd;  // Used to obtain a seed for the random number engine.
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd().
 
    std::uniform_real_distribution<> dis(0.0, 1.0);
    std::vector<unsigned> repeated;

    graph->add_node(nodes[0]);
    repeated.push_back(0);
    graph->add_node(nodes[1]);
    repeated.push_back(1);
    graph->add_edge(0, 1);

    for(unsigned i = 2; i < nodes.size(); ++i){
      double random_value = dis(gen);
      graph->add_node(nodes[i]);
      unsigned tgt = repeated[static_cast<unsigned>(repeated.size() * random_value)];
      graph->add_edge(i, tgt);
      repeated.push_back(i);
      repeated.push_back(tgt);
    }
    auto g_cons_end = std::chrono::high_resolution_clock::now();
    if(print_execution){
      print_execution_time("Graph's construction time", g_cons_end - g_cons_begin);
      std::vector<std::vector<unsigned>> nodes = graph->get_adjacency_list(); 
      auto traversal_begin = std::chrono::high_resolution_clock::now();
      for(unsigned i = 0; i < nodes.size(); ++i){
	for(unsigned j = 0; j < nodes[i].size(); ++j){

	}
      }
      auto traversal_end = std::chrono::high_resolution_clock::now();
      print_execution_time("Graph's traversal time", traversal_end - traversal_begin);
    }
    return graph;
  }
};
#endif
