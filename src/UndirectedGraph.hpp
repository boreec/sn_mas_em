#ifndef __UNDIRECTED_GRAPH_HPP__
#define __UNDIRECTED_GRAPH_HPP__
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

template <class T>
class UndirectedGraph{
  /*
   * Basic class to represent an undirected graph. It contains a vector for
   * every nodes and a 2D vector for the corresponding adjacency matrix. In
   * this implementation, the edges do not carry any information, so they are
   * stored as a boolean in the adjacency matrix (true if there is an edge, false
   * otherwise).
   * Moreover, the generic parameter allow the nodes to store data of a certain type.
   */
private:
  std::vector<std::vector<unsigned>> _adjacency_list;
  std::vector<T> _nodes;
public:
  
  void add_edge(const unsigned n1, const unsigned n2){
    _adjacency_list[n1].push_back(n2);
    _adjacency_list[n2].push_back(n1);
  }

  void add_node(const T t){
    _nodes.push_back(t);
    std::vector<unsigned> new_node;
    _adjacency_list.push_back(new_node);
  }

  std::vector<std::vector<unsigned>> get_adjacency_list() const{
    return _adjacency_list;
  }
  /*
   * Write the graph in file using dot format for any generic type that is not an Agent.
   */
  
  void export_dot(const std::string filename){
    std::ofstream dotfile(filename);

    if(!dotfile.is_open()){
      std::cerr << "Error while attempting to write graph as dotfile (" << filename;
      std::cerr << ")." << std::endl;
      return;
    }

    dotfile << "graph G {\n";
    dotfile << "node [shape=point];\n";

    std::vector<bool> seen_idx(_adjacency_list.size(), false);
    
    for(unsigned i = 0; i < _adjacency_list.size(); ++i){
      seen_idx[i] = true;
      for(unsigned j = 0; j < _adjacency_list[i].size(); ++j){
	if(!seen_idx[_adjacency_list[i][j]]){
	  dotfile << i << " -- " << _adjacency_list[i][j] << ";\n";
	}
      }
    }
    
    dotfile << "}\n";
    dotfile.close();
  }

  /*
   * Return the degree of a node as an unsigned integer.
   */
  unsigned get_node_degree(const unsigned n1) const{

    return _adjacency_list[n1].size();
  }

  /*
   * Return the total amount of degrees (edges) in the graph.
   * (possible optimization : loop on upper (or lower) triangle only.
   */
  unsigned get_nodes_degree() const{
    unsigned degree = 0;
   
    for(auto it = _adjacency_list.begin(); it != _adjacency_list.end(); ++it)
        degree += it->size();

    return degree;
  }
  
  std::string to_string() const{
    if(_adjacency_list.empty()){
      return "graph is empty.";
    }
    std::string result = "";
    
    for(unsigned i = 0; i < _adjacency_list.size(); ++i){
      for(unsigned j = 0; j < _adjacency_list[i].size() - 1; ++j){
	result += std::to_string(_adjacency_list[i][j]) + "\t";
      }
      result += std::to_string(_adjacency_list[i].back()) + "\n";
      }
    return result;
  }

  unsigned size() const{
    return _nodes.size();
  }

  /*
   * Return the vector of nodes connected to a given node.
   */
  std::vector<unsigned> get_neighbours(const unsigned node) const{
    return _adjacency_list[node];
  }

  /*
   * Return a reference to the value stored by a given node.
   */
  T get_node_value(const unsigned node) {
    return _nodes[node];
  }

  void set_node_value(const unsigned node, T t){
    _nodes[node] = t;
  }
};

/*
template <>
void UndirectedGraph<Agent>::export_dot(const std::string filename){

  std::ofstream dotfile(filename);

  if(!dotfile.is_open()){
    std::cerr << "Error while attempting to write graph as dotfile (" << filename;
    std::cerr << ")." << std::endl;
    return;
  }

  dotfile << "graph G {\n";
  dotfile << "node [shape=point];\n";

  std::vector<bool> seen_idx(_adjacency_list.size(), false);
    
  for(unsigned i = 0; i < _adjacency_list.size(); ++i){
    if(_nodes[i].get_sex()){
      dotfile << i << " [color = \"red\"];\n";
    }else{
      dotfile << i << " [color = \"blue\"];\n";
    }
    seen_idx[i] = true;
    for(unsigned j = 0; j < _adjacency_list[i].size(); ++j){
      if(!seen_idx[_adjacency_list[i][j]]){
	dotfile << i << " -- " << _adjacency_list[i][j] << ";\n";
      }
    }
  }
    
  dotfile << "}\n";
  dotfile.close();
}
*/
#endif
